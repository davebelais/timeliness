import re
from copy import copy
from datetime import datetime, date, time, timedelta, timezone, MINYEAR
from numbers import Number
from time import strptime
from typing import Union, Iterable, Sequence, Optional
from calendar import day_name, month_name, day_abbr, month_abbr, LocaleTextCalendar


def tomorrow(
    increment: int = 1,
    today: Optional[date] = None
):
    """
    Retrieves tomorrow's date, or a date *increment* days after *today*.

    :param increment:

        The number of days to look into the future. Defaults to 1 (tomorrow).

    :param today:

        The date to be used as a reference (defaults to today's date).


    """
    if today is None:
        today = date.today()
    return today + timedelta(days=increment)


def yesterday(
    increment: int = 1,
    today: Union[date,None] = date.today()
):
    """
    Retrieves yesterday's date, or a date *increment* days prior to *today*.

    :param increment:

        The number of days to look into the past. Defaults to 1 (yesterday).

    :param today:

        The date to be used as a reference (defaults to today's date).

    :return:

        Yesterday's date, or the date *increment* days before *today*.
    """
    return tomorrow(
        increment = -increment,
        today = today
    )


def date2datetime(
    d: Union[date, datetime],
    tzinfo: Optional[timezone]=None
):
    """
    Convert a *date* to a *datetime*.

    :param d:
        A *date*.

    :param tzinfo:
        The `timezone` in which the `datetime` object should be expressed.

    :return:
        A *datetime*.

    >>> print(date2datetime(date(1999,12,31)))
    1999-12-31 00:00:00

    >>> print(date2datetime(datetime(1999,12,31,23,59,59,999999)))
    1999-12-31 23:59:59.999999

    >>> print(date2datetime(date(1999,12,31),tzinfo=timezone(timedelta(hours=1))))
    1999-12-31 00:00:00+01:00
    """
    if isinstance(d, datetime):
        return d
    elif not isinstance(d, date):
        raise ValueError(
            'The value for *d* must be a *date*.'
        )
    return datetime(
        year = d.year,
        month = d.month,
        day = d.day,
        tzinfo = tzinfo
    )


def datetime2date(dt: Union[datetime,None]):
    """
    >>> print(datetime2date(datetime(1999,12,31,23,59,59,999999)))
    1999-12-31

    >>> print(datetime2date(datetime(1999,12,31)))
    1999-12-31
    """
    if dt is None:
        return None
    if isinstance(dt, str):
        dt = str2datetime(dt)
    return date(year=dt.year, month=dt.month, day=dt.day)


def datetime2time(dt: Union[datetime,date,time,None]):
    """
    >>> print(datetime2time(datetime(1999,12,31,23,59,59,999999)))
    23:59:59.999999
    >>> print(datetime2time(datetime(1999,12,31)))
    00:00:00
    """
    if dt is None:
        return None
    elif isinstance(dt,datetime):
        return dt.time()
    elif isinstance(dt,time):
        return dt
    elif isinstance(dt,date):
        return time(
            hour=0,
            minute=0,
            second=0,
            microsecond=0
        )
    else:
        raise TypeError(
            '*%s* is not a valid type for the parameter *dt*.' %
            (
                dt.__class__.__name__
                if hasattr(dt,'__class__')
                else type(dt).__name__
            )
        )


def time2datetime(dt: Union[time,None]):
    '''
     >>> print(time2datetime(time(23,59,59,999999)))
     0001-01-01 23:59:59.999999

     >>> print(time2datetime(time(0,0,0,1)))
     0001-01-01 00:00:00.000001
     '''
    if isinstance(dt,datetime):
        return dt
    elif isinstance(dt, date):
        return date2datetime(dt)
    elif isinstance(dt,time):
        return datetime(
            year=MINYEAR,
            month=1,
            day=1,
            hour=dt.hour,
            minute=dt.minute,
            second=dt.second,
            microsecond=dt.microsecond
        )
    else:
        return datetime(
            year=0,
            month=1,
            day=1,
            hour=0,
            minute=0,
            second=0,
            microsecond=0
        )




def weekday(
    day: Union[str,int],
    week: Union[int,None] = None,
    today: Union[date,None] = None
):
    """

    :param day:

        A day of the week. Monday-Sunday or 0-6.

    :param week:

        Add this number of weeks to the next occurence of the indicated *day*.

    :param today:

        If specified, use this date as the baseline instead of *date.today()*.

    :return:

        The date on which the indicated weekday occurs.

    :rtype:

        date

    Return the date for the day `week` number of weeks following
    the next occurence of the indicated `day`.

    >>> print(weekday(5,0,date(2014,6,20)))
    2014-06-21

    >>> print(weekday(2,0,date(2014,6,20)))
    2014-06-25

    >>> print(weekday(4,0,date(2014,6,20)))
    2014-06-20

    >>> print(weekday(5,1,date(2014,6,20)))
    2014-06-28

    >>> print(weekday(2,2,date(2014,6,20)))
    2014-07-09

    >>> print(weekday(4,3,date(2014,6,20)))
    2014-07-11
    """
    ve = ValueError(
        ('%s is not a valid argument.' % repr(day)) +
        'Acceptable arguments are ' +
        'are 0-6 or Monday-Sunday.'
    )
    if isinstance(day, str):
        weekdays = [
            'mon',
            'tue',
            'wed',
            'thu',
            'fri',
            'sat',
            'sun'
        ]
        try:
            day = weekdays.index(
                day.lower().strip()[:3]
            )
        except KeyError:
            raise ve
    elif isinstance(day, Number):
        day = int(day)
    else:
        raise ve
    if week is None:
        week = 0
    if today is None:
        today = date.today()
    elif not isinstance(today,date):
        raise TypeError(
            'The parameter *today* must be a *date* object.'
        )
    n = 0
    t = today.weekday()
    if day < t:
        n = 7 - t + day
    else:
        n = day - t
    return today + timedelta(days = n + (7 * week))


def yy2yyyy(yy: Union[str,int]):
    """
    Convert a 2-digit date representation to the closest year ending in these 2 digits.
    """
    yy = int(yy)
    if yy > 99 or abs(date.today().year) < 100:
        return yy
    y = date.today().year
    century = int(float(y) / 100.0) * 100
    this_century = century + yy
    next_century = century + 100 + yy
    previous_century = century + yy
    yyyy = this_century
    difference = abs(this_century - y)
    if abs(next_century - y) < difference:
        yyyy = next_century
        difference = abs(next_century - y)
    if abs(previous_century - y) < difference:
        yyyy = previous_century
        difference = abs(previous_century - y)
    return yyyy


def md2ymd(month: Union[str,int], day: Union[str,int], today: Union[date,str,None] = None):
    """
    Return the nearest date to *today* where *month* is the month and *day* is the day of the month.

    >>> print(md2ymd(1,1,date(1999,10,4)))
    2000-01-01

    >>> print(md2ymd(11,20,date(1999,1,4)))
    1998-11-20

    >>> print(md2ymd(2,28,date(1999,1,4)))
    1999-02-28

    Leap year

    >>> print(md2ymd(2,29,date(1999,1,4)))
    2000-02-29
    """
    if not isinstance(today,date):
        if today is None:
            today = date.today()
        elif isinstance(today,(str,bytes)):
            today = str2date(today)
        elif isinstance(today,datetime):
            today = datetime2date(today)
        else:
            raise TypeError(
                '*%s* is not a valid input type for the parameter *today*.' % (
                    today.__class__
                    if hasattr(today,'__class__')
                    else type(today)
                ).__name__
            )
    month = int(month)
    day = int(day)
    try:
        nearest_ymd = date(today.year, month, day)
        minimum_delta = nearest_ymd - today if nearest_ymd > today else today - nearest_ymd
    except ValueError:
        nearest_ymd = None
    last_year = today.year - 1
    next_year = today.year + 1
    if month == 2 and day == 29:
        minimum_delta = timedelta(days=365 * 4 + 1)
        last_leap_year = last_year
        last_year = None
        while last_year is None:
            try:
                last_year = date(last_leap_year, month, day).year
            except ValueError as e:
                last_leap_year -= 1
        next_leap_year = next_year
        next_year = None
        while next_year is None:
            try:
                next_year = date(next_leap_year, month, day).year
            except ValueError as e:
                next_leap_year -= 1
    for y in (
        last_year,
        next_year
    ):
        ymd = date(y, month, day)
        td = ymd - today if ymd > today else today - ymd
        if td < minimum_delta:
            minimum_delta = td
            nearest_ymd = ymd
    return nearest_ymd


def str2date(d: Union[str,date,datetime]):
    """
    Parse a text representation of a date, and return a *date* object.

    :param d:

        A text representation of a date.

    >>> print(str2date('Sunday December 21st 1999 5:20:55.01 pm'))
    1999-12-21
    """
    return str2juncture(d,output=date)


def str2datetime(dt: Union[str,date,datetime]):
    """
    Accepts a text representation of a date, and outputs a *datetime* object.

    >>> print(str2date('Sunday December 21st 1999 5:20:55.01 pm'))
    1999-12-21
    """
    return str2juncture(dt, output=datetime)


def str2juncture(
    juncture: Union[str, datetime, date, time],
    output: Union[Sequence, type(object)] = (datetime, date, time)
):
    """
    Accepts a datetime or date string and outputs a python datetime [or date] object.
    The output defaults to a datetime, but auto-selection can be enabled by passing a tuple (datetime,date) to the output keyword.

    :param junction:

    :param output:
        A type or tuple specifying acceptable output formats - date, datetime, or either - specified by a tuple:
        (date,datetime).

    >>> print(str2juncture('2014-03-27-00.33.20.917994'))
    2014-03-27 00:33:20.917994

    >>> print(str2juncture('2014-03-27T07:33:20.917994'))
    2014-03-27 07:33:20.917994

    >>> print(str2juncture('Tue Aug 16 21:30:00 1988'))
    1988-08-16 21:30:00

    >>> print(str2juncture('Di 16 Aug 21:30:00 1988'))
    1988-08-16 21:30:00

    >>> print(str2juncture('Friday 1/3/2014',output=datetime)) # U.S. format
    2014-01-03 00:00:00

    >>> print(str2juncture('Friday 3.1.2014')) # Euro format, detected by looking up the weekday against 2 possible interpretations.
    2014-01-03

    >>> print(str2juncture('Tuesday 1/3/11')) # U.S. format
    2011-03-01

    >>> print(str2juncture('Tuesday 3/1/11')) # Euro format, detected by looking up the weekday against 2 possible interpretations.
    2011-03-01

    >>> print(str2juncture('Tuesday 11/3/1')) # Format auto-detected using the weekday to validate.
    2011-03-01

    >>> print(str2juncture('Tuesday 11/1/3')) # Format auto-detected using the weekday to validate.
    2011-03-01

    >>> print(str2juncture('11/3/1')) # Format auto-selected without using the weekday to validate.
    2001-11-03

    >>> print(str2juncture('11/1/3')) # Format auto-selected without using the weekday to validate.
    2003-11-01

    >>> print(str2juncture('April 3rd, 2011'))
    2011-04-03

    >>> print(str2juncture('3/3/11'))
    2011-03-03

    >>> print(str2juncture('Sunday December 21st 1999 5:20:55.01 pm'))
    1999-12-21 17:20:55.010000

    >>> print(str2juncture('5/12/1999'))
    1999-05-12

    >>> print(str2juncture('13/12/1999'))
    1999-12-13

    >>> print(str2juncture('5/12/99'))
    1999-05-12

    >>> print(str2juncture('13/12/99'))
    2099-12-13

    >>> print(str2juncture('Sunday December 21st 1999 5:20:55.01 pm',output=date)) # Forced to output date format in lieu of datetime
    1999-12-21

    >>> print(str2juncture('Sunday December 21st 1999',output=(date,datetime))) # Auto-selects output format when passed a tuple: (date,time)
    1999-12-21

    >>> print(str2juncture('Sunday December 21st 1999')) # Forces datetime format by default
    1999-12-21

    >>> print(str2juncture('1999-12-21 00:00:00',output=[datetime,date]))
    1999-12-21 00:00:00

    >>> print(str2juncture('1999-12-21',output=[datetime,date]))
    1999-12-21

    >>> print(str2juncture('1:45:00'))
    01:45:00

    >>> print(str2juncture('1:45'))
    01:45:00

    >>> print(str2juncture('1:45'))
    01:45:00

    >>> print(str2juncture('1997-07-16T19:20:30.45+01:00'))
    1997-07-16 19:20:30.450000+01:00
    """
    if isinstance(juncture,str):
        j = juncture
    else:
        j = copy(juncture)
    if not isinstance(j, (str, date, datetime, time)):
        raise TypeError(
            '%s is not a valid type for the parameter *junction*. ' % (
                j.__class__.__name__
                if hasattr(j, '__class__')
                else type(j).__name__
            )
        )
    if not isinstance(output,tuple):
        if isinstance(output,Iterable):
            output = tuple(output)
        elif output in (date,datetime,time):
            output = (output,)
        else:
            raise TypeError(
                (
                    '%s is not a valid type for *output*. ' % (
                        output.__class__.__name__
                        if hasattr(output,'__class__')
                        else type(output).__name__
                    )
                ) +
                'Valid values for *output* are *date*, *datetime*, and *time*, or a tuple of any combination of these.'
            )
    if isinstance(j, (datetime, date)):
        if (
            isinstance(j,datetime) and
            (datetime not in output) and
            (date in output)
        ):
            return datetime2date(j)
        elif (
            isinstance(j,date) and
            (date not in output) and
            (datetime in output)
        ):
            return date2datetime(j)
        else:
            return j
    if not isinstance(j, str):
        raise TypeError(
            'The parameter *junction* must be a *str*, *date*, *datetime*, *time*, or *bytes* object.'
        )
    result = None
    try:  # python datetime
        result = datetime.strptime(j, '%Y-%m-%d %X')
    except ValueError:
        try:  # ISO datetime
            result = datetime.strptime(j, '%Y-%m-%dT%X')
        except ValueError:
            try:  # DB2 weirdo format
                result = datetime.strptime(j, '%Y-%m-%d-%H.%M.%S.%f')
            except ValueError:
                try:  # universal date
                    result = datetime.strptime(j, '%Y-%m-%d')
                    if date in output:
                        result = datetime2date(result)
                except ValueError:
                    try:  # locale datetime
                        result = datetime.strptime(j, '%c')
                    except ValueError:
                        try:  # locale date
                            result = datetime.strptime(j, '%x')
                            if date in output:
                                result = datetime2date(result)
                        except ValueError:
                            try:  # locale time
                                result = datetime.strptime(j, '%X')
                                if time in output:
                                    result = datetime2time(result)
                            except ValueError:
                                pass
    if result is None:
        weekdays = [
            'mon',
            'tue',
            'wed',
            'thu',
            'fri',
            'sat',
            'sun'
        ]
        months = [
            'jan',
            'feb',
            'mar',
            'apr',
            'may',
            'jun',
            'jul',
            'aug',
            'sep',
            'oct',
            'nov',
            'dec'
        ]
        now = datetime.now()
        datetime_kwargs = dict()
        time_found = True if TIME_RE.search(j) else False
        t = None
        try:
            t = str2time(j)
            for attr in ['hour', 'minute', 'second', 'microsecond', 'tzinfo']:
                datetime_kwargs[attr] = getattr(t, attr)
        except ValueError as ve:
            pass
        j = TIME_RE.sub('',j).strip()
        if j:
            try:  # universal date representation
                dt = datetime.strptime(j, '%Y-%m-%d')
                datetime_kwargs['year'] = dt.year
                datetime_kwargs['month'] = dt.month
                datetime_kwargs['day'] = dt.day
            except ValueError as e:
                try:  # locale's appropriate date representation
                    dt = datetime.strptime(j, '%x')
                    datetime_kwargs['year'] = dt.year
                    datetime_kwargs['month'] = dt.month
                    datetime_kwargs['day'] = dt.day
                except ValueError as e:
                    day = None
                    weekday = None
                    for wd in weekdays:
                        found = re.findall(r'\b%s[a-z]*' % wd, j, flags=re.IGNORECASE)
                        if found:
                            weekday = weekdays.index(wd)
                            break
                    month = None
                    for m in months:
                        wd_re = re.compile(r'\b%s[a-z]*' % m, flags=re.IGNORECASE)
                        found = wd_re.findall(j)
                        if found:
                            month = months.index(m) + 1
                            break
                    year = None
                    year_re = re.compile(r'\b\d\d\d\d?\b', flags=re.IGNORECASE)
                    found = year_re.findall(j)
                    if found:
                        year = int(found[0])
                    slashdate_str = None
                    slashdate_re = re.compile(r'\b\d+/\d+(?:/\d+)?', flags=re.IGNORECASE)
                    found = slashdate_re.findall(j)
                    date_parts = None
                    if found:
                        slashdate_str = found[0]
                        date_parts = slashdate_str.split('/')
                    else:
                        date_parts = re.findall(r'\d+', j, flags=re.IGNORECASE)
                    if date_parts:
                        date_parts = [int(ns) for ns in date_parts]
                        if year is not None and year in date_parts:
                            date_parts.pop(date_parts.index(year))
                        if len(date_parts) + (0 if year is None else 1) + (0 if month is None else 1) + (
                            0 if day is None else 1) < 3:
                            if (month is None) and (day is None) and (year is not None):
                                year = now.year
                            if len(date_parts) == 1:
                                if day is None:
                                    if not month:
                                        month = now.month
                                    day = date_parts.pop()
                                elif not month:
                                    month = date_parts.pop()
                        if year is None:
                            dp = []
                            for i in range(len(date_parts)):
                                if date_parts[i] > 31 or (day is not None and date_parts[i] > 12):
                                    year = yy2yyyy(date_parts[i])
                                else:
                                    dp.append(date_parts[i])
                            date_parts = dp
                        if year is not None:
                            dp = []
                            for i in range(len(date_parts)):
                                if date_parts[i] > 12:
                                    day = date_parts[i]
                                else:
                                    dp.append(date_parts[i])
                            date_parts = dp
                        if len(date_parts) == 1:
                            if (month is not None) and (day is not None) and (year is None):
                                year = date_parts.pop()
                            elif (month is not None) and (year is not None) and (day is None):
                                day = date_parts.pop()
                            elif (day is not None) and (year is not None) and (month is None):
                                month = date_parts.pop()
                        elif len(date_parts) == 2:
                            if year is not None:
                                if (
                                    (
                                        datetime(
                                            year=year,
                                            month=date_parts[1],
                                            day=date_parts[0]
                                        ).weekday() == weekday
                                    ) and not (
                                        datetime(
                                            year=year,
                                            month=date_parts[0],
                                            day=date_parts[1]
                                        ).weekday() == weekday
                                    )
                                ):
                                    month = date_parts.pop()
                                    day = date_parts.pop()
                                else:
                                    day = date_parts.pop()
                                    month = date_parts.pop()
                            elif month is not None:
                                if (
                                    (
                                        datetime(
                                            year=date_parts[0],
                                            month=month,
                                            day=date_parts[1]
                                        ).weekday() == weekday
                                    ) and not (
                                        datetime(
                                            year=date_parts[1],
                                            month=month,
                                            day=date_parts[0]
                                        ).weekday() == weekday
                                    )
                                ):
                                    day = date_parts.pop()
                                    year = date_parts.pop()
                                else:
                                    year = date_parts.pop()
                                    day = date_parts.pop()
                            elif day is not None:
                                if (
                                        (datetime(year=yy2yyyy(date_parts[0]), month=date_parts[1],
                                            day=day).weekday() == weekday)
                                    and not (datetime(year=yy2yyyy(date_parts[1]), month=date_parts[0],
                                        day=day).weekday() == weekday)
                                ):
                                    month = date_parts.pop()
                                    year = yy2yyyy(date_parts.pop())
                                else:
                                    year = yy2yyyy(date_parts.pop())
                                    month = date_parts.pop()
                        elif len(date_parts) == 3:
                            if weekday is None:
                                if (  # US - mdy
                                    0 < date_parts[0] <= 12 and
                                    0 < date_parts[1] <= 31
                                ):
                                    year = yy2yyyy(date_parts.pop())
                                    day = date_parts.pop()
                                    month = date_parts.pop()
                                elif ( # Europe - dmy
                                    0 < date_parts[0] <= 12 and
                                    0 < date_parts[1] <= 31
                                ):
                                    year = yy2yyyy(date_parts.pop())
                                    month = date_parts.pop()
                                    day = date_parts.pop()
                                elif ( # International - ymd
                                    0 < date_parts[1] <= 12 and
                                    0 < date_parts[2] <= 31
                                ):
                                    day = date_parts.pop()
                                    month = date_parts.pop()
                                    year = yy2yyyy(date_parts.pop())
                                elif ( # ydm
                                    0 < date_parts[2] <= 12 and
                                    0 < date_parts[1] <= 31
                                ):
                                    month = date_parts.pop()
                                    day = date_parts.pop()
                                    year = yy2yyyy(date_parts.pop())
                            else:
                                if ( # US - mdy
                                    0 < date_parts[0] <= 12 and
                                    0 < date_parts[1] <= 31 and
                                    datetime(
                                        yy2yyyy(date_parts[2]),
                                        date_parts[0],
                                        date_parts[1]
                                    ).weekday() == weekday
                                ):
                                    year = yy2yyyy(date_parts.pop())
                                    day = date_parts.pop()
                                    month = date_parts.pop()
                                elif (  # European - dmy
                                    0 < date_parts[0] <= 12 and
                                    0 < date_parts[1] <= 31 and
                                    datetime(
                                        yy2yyyy(date_parts[2]),
                                        date_parts[1],
                                        date_parts[0]
                                    ).weekday() == weekday
                                ):
                                    year = yy2yyyy(date_parts.pop())
                                    month = date_parts.pop()
                                    day = date_parts.pop()
                                elif (  # International - ymd
                                    0 < date_parts[1] <= 12 and
                                    0 < date_parts[2] <= 31 and
                                    datetime(
                                        yy2yyyy(date_parts[0]),
                                        date_parts[1],
                                        date_parts[2]
                                    ).weekday() == weekday
                                ):
                                    day = date_parts.pop()
                                    month = date_parts.pop()
                                    year = yy2yyyy(date_parts.pop())
                                elif ( # ydm
                                    0 < date_parts[2] <= 12 and
                                    0 < date_parts[1] <= 31 and
                                    datetime(
                                        yy2yyyy(date_parts[0]),
                                        date_parts[2],
                                        date_parts[1]
                                    ).weekday() == weekday
                                ):
                                    month = date_parts.pop()
                                    day = date_parts.pop()
                                    year = yy2yyyy(date_parts.pop())
                        if year is not None:
                            dp = []
                            for i in range(len(date_parts)):
                                if date_parts[i] > 12:
                                    day = date_parts[i]
                                else:
                                    dp.append(date_parts[i])
                            date_parts = dp
                        if len(date_parts):
                            if year is None:
                                year = date_parts.pop()
                            if month is None:
                                month = date_parts.pop()
                            if day is None:
                                day = date_parts.pop()
                    datetime_kwargs['year'] = year
                    datetime_kwargs['month'] = month
                    datetime_kwargs['day'] = day
            if (
                datetime in output and (
                    'hour' in datetime_kwargs or
                    date not in output
                )
            ):
                result = datetime(**datetime_kwargs)
            elif (
                date in output and (
                    'hour' not in datetime_kwargs or
                    datetime not in output
                )
            ):
                if datetime_kwargs:
                    for k,v in copy(datetime_kwargs).items():
                        if (
                            (k not in ('year', 'month', 'day'))
                            or (v is None)
                        ):
                            del datetime_kwargs[k]
                    result = (
                        date(
                            **datetime_kwargs
                        ) if datetime_kwargs
                        else None
                    )
                else:
                    result = None
        else:
            if (
                (t is not None) and
                (not j) and
                (time in output)
            ):
                result = t
            else:
                raise ValueError(
                    'No *date*, *time*, or *datetime* could be parsed from %s.' % repr(juncture)
                )
    if isinstance(result,datetime):
        if (datetime not in output):
            if date in output:
                result = datetime2date(result)
            elif time in output:
                result = datetime2time(result)
    elif isinstance(result,date):
        if date not in output:
            if datetime in output:
                result = date2datetime(result)
            elif time in output:
                result = time(0,0,0,0)
    return result


TIME_RE = re.compile(
    (
        r'%(tre)s\s*(am|pm)?\s*(?:([−\-\+Zz])\s*%(tre)s)?' % dict(
            tre=r'(\d+)\s*(?:\:\s*(\d+)\s*)(?:\:\s*(\d+(?:\.\d+)?))?'
        )
    ),
    flags=re.IGNORECASE
)


def str2time(t: Union[str,time]):
    """
    https://en.wikipedia.org/wiki/ISO_8601#Time_zone_designators

    :param string:
    :return:

    >>> print(str2time('07:33:20.917994'))
    07:33:20.917994

    >>> print(str2time('07:33:20.917994'))
    07:33:20.917994

    >>> print(str2time('0:0:0.000000'))
    00:00:00
    """
    if isinstance(t, time):
        return t
    now = datetime.now()
    time_kwargs = dict(
        hour=0,
        minute=0,
        second=0,
        microsecond=0
    )
    found = TIME_RE.search(t)
    if found:
        m = float(10**6)
        hour, minute, second, meridiem, offset, offset_hours, offset_minutes, offset_seconds = found.groups()
        microsecond = 0
        hour = int(hour)
        if meridiem is not None:
            meridiem = meridiem.lower()
            if meridiem == 'am':
                if hour == 12:
                    hour = 0
            elif meridiem == 'pm':
                if hour != 12:
                    hour += 12
        minute = int(minute or 0)
        if second is None:
            second = 0
        else:
            second, microsecond = divmod(float(second) * m, m)
            second = int(second)
            microsecond = int(microsecond)
        tz = None
        if offset:
            offset_microseconds = 0
            if offset_hours is None:
                offset_hours = 0
            else:
                offset_hours = int(offset_hours)
            if offset_minutes is None:
                offset_minutes = 0
            else:
                offset_minutes = int(offset_minutes)
            if offset_seconds is None:
                offset_seconds = 0
            else:
                offset_seconds, offset_microseconds = divmod(float(offset_seconds) * m,m)
                offset_seconds = int(offset_seconds)
                offset_microseconds = int(offset_microseconds)
            if (offset_seconds or offset_microseconds):
                # The *timezone* requires whole minutes, so we round to the nearest minute
                seconds_offset = timedelta(seconds=offset_seconds,microseconds=offset_microseconds)
                if (
                    timedelta(minutes=1)-seconds_offset <=
                    seconds_offset
                ):
                    offset_minutes += 1
            td = timedelta(
                hours=offset_hours,
                minutes=offset_minutes
            )
            if offset in '−-':
                td = -td
            tz = timezone(td)
        return time(hour, minute, second, microsecond, tz)
    else:
        raise ValueError(
            '%s could not be parsed as a *time*.' % repr(t)
        )


if __name__ == '__main__':
    import doctest
    doctest.testmod()
